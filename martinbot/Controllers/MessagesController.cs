﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using martinbot.Dialogs;
using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace martinbot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {
                await Conversation.SendAsync(activity, () => new Dialogs.RootDialog());
            }
            else
            {
                await HandleSystemMessage(activity);
            }
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        private async Task HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                Func<ChannelAccount, bool> isChatbot = channelAcct => channelAcct.Id == message.Recipient.Id;
                if (message.MembersAdded?.Any(isChatbot) ?? false)
                {
                    ConnectorClient client = new ConnectorClient(new Uri(message.ServiceUrl));
                    var reply = message.CreateReply();
                    reply.Attachments = new List<Attachment>
                    {
                        new Attachment
                        {
                            Name = "Acme Inc. Logo",
                            ContentType = "image/png",
                            ContentUrl = Configs.BASE_URL_ASSETS_PUBLIC + "/images/acme-logo.png",
                        }
                    };

                    await client.Conversations.ReplyToActivityAsync(reply);

                    reply = Replies.WelcomeReply(message);
                    await client.Conversations.ReplyToActivityAsync(reply);
                }
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
            }
            else if (message.Type == ActivityTypes.Typing)
            {
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }
        }
    }
}