﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

using martinbot.Models;

namespace martinbot.Controllers
{
    public class PingController : ApiController
    {   
        public HttpResponseMessage Get()
        {
           string path = System.Web.Hosting.HostingEnvironment.MapPath("~/ping.tmpl");
           var tmpl = System.IO.File.ReadAllText(path);
           tmpl = tmpl.Replace("{{BASE_URL_ASSETS_PUBLIC}}", Configs.BASE_URL_ASSETS_PUBLIC);

           HttpResponseMessage m = new HttpResponseMessage(HttpStatusCode.OK);
           m.Content = new StringContent(tmpl);
           m.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
           return m;
        }
    }
}