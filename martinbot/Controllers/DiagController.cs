﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

using martinbot.Models;

namespace martinbot.Controllers
{
    public class DiagController : ApiController
    {
        public HttpResponseMessage Get()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("diagnostics <br/>");
            sb.Append("now: " + System.DateTime.Now.ToString() + "<br>");
            sb.Append("botid:" + Configs.BOT_ID + "<br>");

            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/diag.tmpl");
            var tmpl = System.IO.File.ReadAllText(path);
            tmpl = tmpl.Replace("{{DIAGMESSAGE}}", sb.ToString());

            HttpResponseMessage m = new HttpResponseMessage(HttpStatusCode.OK);
            m.Content = new StringContent(tmpl);
            m.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return m;
        }
    }
}