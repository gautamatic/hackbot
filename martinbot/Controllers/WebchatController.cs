﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

using martinbot.Models;

namespace martinbot.Controllers
{
    public class WebchatController : ApiController
    {
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                string botId = Configs.BOT_ID;
                string tempToken = await GenerateWebchatTempToken(Configs.WEBCHAT_S);
                tempToken = Regex.Replace(tempToken, "^\"|\"$", "");

                string webchatIframe = $"<iframe src='https://webchat.botframework.com/embed/{botId}?t={tempToken}'></iframe>";
                var response = Request.CreateResponse(HttpStatusCode.OK, webchatIframe);
                response.Content = new StringContent(webchatIframe, Encoding.UTF8, "text/html");
                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message, ex);
                return response;
            }
        }

        private async Task<string> GenerateWebchatTempToken(string webchatSecretKey)
        {
            var tempToken = "";
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, new Uri(Configs.WEBCHAT_TEMP_TOKEN_ENDPOINT));
            req.Headers.Add("Authorization", "BOTCONNECTOR " + Configs.WEBCHAT_S);

            using (var client = new HttpClient())
            {
                HttpResponseMessage resp = await client.SendAsync(req);
                tempToken = await resp.Content.ReadAsStringAsync();
            }
            return tempToken;
        }

    }
}