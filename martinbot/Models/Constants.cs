﻿namespace martinbot.Models
{
    public static class Constants
    {
        public static float MIN_CONFIDENCE_LEVEL = 0.5f;

        public static string INTENT_CHITCHAT_CURRENT_TIME = "chitchat-current-time";
        public static string INTENT_CHITCHAT_CURRENT_DATE = "chitchat-current-date";

        public static string INTENT_KNOWLEDGE = "knowledge";
        public static string INTENT_ACME_SERVICES = "acme-services";
        public static string INTENT_ACME_LOCATION = "acme-location";

        public static string INTENT_USER_HELP = "user-help";
    }
}