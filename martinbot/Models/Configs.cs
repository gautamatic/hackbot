﻿using System.Configuration;

namespace martinbot.Models
{
    public static class Configs
    {
        public static string BOT_ID = ConfigurationManager.AppSettings["BotId"].ToString();

        public static string WEBCHAT_S = ConfigurationManager.AppSettings["webchatS"].ToString();
        public static string WEBCHAT_TEMP_TOKEN_ENDPOINT = ConfigurationManager.AppSettings["webchatTempTokenEndpoint"].ToString();

        public static string WATCONV_UN = ConfigurationManager.AppSettings["watConvUn"].ToString();
        public static string WATCONV_PW = ConfigurationManager.AppSettings["watConvPw"].ToString();
        public static string WATCONV_VER = ConfigurationManager.AppSettings["watConvVer"].ToString();
        public static string WATCONV_WKS_ID = ConfigurationManager.AppSettings["watConvWksId"].ToString();

        public static string BASE_URL_ASSETS_PUBLIC = ConfigurationManager.AppSettings["baseUrlAssetsPublic"].ToString();

        public static string CNSTRING_STORAGE = ConfigurationManager.ConnectionStrings["cnStringStorage"].ToString();
    }
}