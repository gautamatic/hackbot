﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

using wc = IBM.WatsonDeveloperCloud.Conversation.v1;
using wcm = IBM.WatsonDeveloperCloud.Conversation.v1.Model;
using martinbot.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace martinbot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        private dynamic wcContext = null;

        public class WcEntity
        {
            public string entity;
            public string value;
            public float confidence;
        }

        public async Task StartAsync(IDialogContext context)
        {
            try
            {
                wcm.MessageRequest req = new wcm.MessageRequest { Input = new wcm.InputData { Text = "" } };
                var wcClient = new wc.ConversationService(Configs.WATCONV_UN, Configs.WATCONV_PW, Configs.WATCONV_VER);
                var resp = wcClient.Message(Configs.WATCONV_WKS_ID, req);
                this.wcContext = resp.Context;
                context.Wait(MessageReceivedAsync);
            }
            catch(Exception ex)
            {
                await context.PostAsync("ERR:" + ex.Message);
            }
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> msg)
        {
            var activity = await msg as Activity;
            ConnectorClient client = new ConnectorClient(new Uri(activity.ServiceUrl));
            var reply = activity.CreateReply();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string inputText = "";
            string outputText = "";
            string intent = "";
            float intentConfidence = 0f;

            try
            {
                if (this.wcContext != null)
                    inputText = activity.Text;

                wcm.MessageRequest req = new wcm.MessageRequest { Input = new wcm.InputData { Text = inputText } };
                req.Context = this.wcContext;
                var wcClient = new wc.ConversationService(Configs.WATCONV_UN, Configs.WATCONV_PW, Configs.WATCONV_VER);
                var resp = wcClient.Message(Configs.WATCONV_WKS_ID, req);

                if (resp.Intents.Count > 0)
                {
                    intent = resp.Intents[0]["intent"].ToString();
                    intentConfidence = float.Parse(resp.Intents[0]["confidence"].ToString());
                }

                List<WcEntity> lstEntities = new List<WcEntity>();
                foreach (var entityItem in resp.Entities)
                {
                    WcEntity t = new WcEntity();
                    t.entity = entityItem["entity"]?.ToString() ?? "";
                    t.value = entityItem["value"]?.ToString() ?? "";
                    t.confidence = float.Parse(entityItem["confidence"]?.ToString() ?? 0f);
                    lstEntities.Add(t);
                }

                if (resp.Output["text"].Count > 0)
                    outputText = resp.Output["text"][0].ToString();

                reply.Text = outputText;

                if (intent == Constants.INTENT_CHITCHAT_CURRENT_TIME && intentConfidence > Constants.MIN_CONFIDENCE_LEVEL)
                    reply = Replies.CurrentTimeReply(activity, context, outputText);

                if (intent == Constants.INTENT_CHITCHAT_CURRENT_DATE && intentConfidence > Constants.MIN_CONFIDENCE_LEVEL)
                    reply = Replies.CurrentDayReply(activity, context, outputText);

                if (intent == Constants.INTENT_KNOWLEDGE && lstEntities.Any(e => e.entity == "topic" && e.value == "acme" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.AcmeReply(activity, context, outputText);

                if (intent == Constants.INTENT_KNOWLEDGE && lstEntities.Any(e => e.entity == "topic" && e.value == "ml" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.MachineLearningKnowledgeReply(activity, context, outputText);

                if (intent == Constants.INTENT_ACME_SERVICES && lstEntities.Any(e => e.entity == "topic" && (e.value == "yourself" || e.value == "acme")))
                    reply = Replies.ServicesReply(activity, context, outputText);


                if (intent == Constants.INTENT_ACME_SERVICES && lstEntities.Any(e => e.entity == "servicetype" && e.value == "machine-learning" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.MachineLearningServiceReply(activity, context, outputText);

                if (intent == Constants.INTENT_ACME_SERVICES && lstEntities.Any(e => e.entity == "servicetype" && e.value == "cloud-migration" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.CloudMigrationServiceReply(activity, context, outputText);

                if (intent == Constants.INTENT_ACME_SERVICES && lstEntities.Any(e => e.entity == "servicetype" && e.value == "cybersecurity" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.CyberSecurityServiceReply(activity, context, outputText);

                if (intent == Constants.INTENT_ACME_SERVICES && lstEntities.Any(e => e.entity == "servicetype" && e.value == "software-development" && e.confidence > Constants.MIN_CONFIDENCE_LEVEL))
                    reply = Replies.SoftwareDevelopmentServiceReply(activity, context, outputText);

                if (intent == Constants.INTENT_ACME_LOCATION && intentConfidence > Constants.MIN_CONFIDENCE_LEVEL)
                    reply = Replies.AddressReply(activity, context, outputText);

                if (intent == Constants.INTENT_USER_HELP && intentConfidence > Constants.MIN_CONFIDENCE_LEVEL)
                    reply = Replies.HelpReply(activity, context, outputText);


                this.wcContext = resp.Context;
                context.Wait(MessageReceivedAsync);
            }
            catch (Exception ex)
            {
                reply = activity.CreateReply("ERR: " + ex.Message);
            }
            finally
            {
                await client.Conversations.ReplyToActivityAsync(reply);
            }
        }
    }
}