﻿using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity SoftwareDevelopmentServiceReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            var thumbnailCard = new ThumbnailCard
            {
                Title = "Software Development with Acme Inc.",
                Subtitle = "Machine Learning, AU and Process Automation are central to our software development efforts.",
                Text = answerText,
                Images = new List<CardImage> {new CardImage(Configs.BASE_URL_ASSETS_PUBLIC + "/images/services-dev.png") },
                Buttons = new List<CardAction>
                {
                    new CardAction { Title = "learn more »", Type = ActionTypes.OpenUrl, Value = "http://www.acme.com/services/software-development"}
                }

            };
            reply.Attachments = new List<Attachment> { thumbnailCard.ToAttachment()};
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Help", Type = ActionTypes.ImBack, Value = "Help"},
                    new CardAction { Title = "e.g. I want to contact Acme Inc.", Type = ActionTypes.ImBack, Value = "I want to contact Acme Inc."}
                }
            };

            return reply;
        }
    }
}