﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity ServicesReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.Text = answerText;
            reply.TextFormat = TextFormatTypes.Markdown;
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Can u help with ML?", Type = ActionTypes.ImBack, Value = "Can u help with ML?"},
                    new CardAction { Title = "e.g. I need experts for Cloud Migration", Type = ActionTypes.ImBack, Value = "I need experts for Cloud Migration"},
                    new CardAction { Title = "e.g. What can u do in Cybersecurity?", Type = ActionTypes.ImBack, Value = "What can u do in Cybersecurity?"},
                    new CardAction { Title = "e.g. Can u develop custom enterprise software?", Type = ActionTypes.ImBack, Value = "Can u develop custom enterprise software?"},
                }
            };

            return reply;
        }
    }
}