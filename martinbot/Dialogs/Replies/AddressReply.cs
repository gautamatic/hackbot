﻿using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Web.Hosting;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity AddressReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            var heroCard = new HeroCard
            {
                Title = "Acme Inc. Offices",
                Subtitle = answerText,

                Images = new List<CardImage>
                {
                    new CardImage(Configs.BASE_URL_ASSETS_PUBLIC + "/images/map.png")
                },
                Buttons = new List<CardAction>
                {
                    new CardAction
                    {
                        Type = ActionTypes.OpenUrl,
                        Title = "view map »",
                        Value = "https://www.google.com/maps/place/5+Middlesex+Ave+%23400,+Somerville,+MA+02145/@42.3915469,-71.1173873,13z/data=!4m5!3m4!1s0x89e370d9092be301:0x1bba77a67a245fa6!8m2!3d42.3915469!4d-71.0823684"
                    }
                }
            };

            reply.Attachments = new List<Attachment>
            {
                heroCard.ToAttachment()
            };

            return reply;
        }
    }
}