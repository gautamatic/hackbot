﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity HelpReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply(answerText);
            reply.TextFormat = TextFormatTypes.Markdown;

            reply.Text = answerText;
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. What's Acme Inc.?", Type = ActionTypes.ImBack, Value = "What's Acme Inc.?"},
                    new CardAction { Title = "e.g. What services do u offer?", Type = ActionTypes.ImBack, Value = "What services do u offer?"},
                    new CardAction { Title = "e.g. Where's ur office?", Type = ActionTypes.ImBack, Value = "Where's ur office?"},
                    new CardAction { Title = "e.g. Tell me about machine learning", Type = ActionTypes.ImBack, Value = "Tell me about machine learning"},
                    new CardAction { Title = "e.g. Can u help with cloud migration?", Type = ActionTypes.ImBack, Value = "Can u help with cloud migration?"},
                    new CardAction { Title = "e.g. Could u help with RPA?", Type = ActionTypes.ImBack, Value = "Could u help with RPA?"},
                    new CardAction { Title = "e.g. Explain AI", Type = ActionTypes.ImBack, Value = "Explain AI"},
                    new CardAction { Title = "e.g. I need your phone no", Type = ActionTypes.ImBack, Value = "I need your phone no"},
                    new CardAction { Title = "e.g. What time is it?", Type = ActionTypes.ImBack, Value = "What time is it?"},
                }
            };

            return reply;
        }
    }
}