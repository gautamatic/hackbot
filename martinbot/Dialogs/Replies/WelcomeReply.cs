﻿using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity WelcomeReply(Activity activity)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            reply.Text = "Hello! I am a chatbot powered by Acme Inc.. <br/>" +
                "I can answer questions about Acme Inc. and its services. <br/>" +
                "Just type 'help' if you feel lost :-) <br/>" +
                "So, how may I assist you today?";

            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Help", Type = ActionTypes.ImBack, Value = "Help"},
                    new CardAction { Title = "e.g. Who's Acme Inc.?", Type = ActionTypes.ImBack, Value = "Who's Acme Inc.?"},
                    //new CardAction { Title = "e.g. Where's ur office?", Type = ActionTypes.ImBack, Value = "Where's ur office?"},
                    //new CardAction { Title = "e.g. What's machine learning?", Type = ActionTypes.ImBack, Value = "What's machine learning?"},
                    //new CardAction { Title = "e.g. I need experts in cloud migration", Type = ActionTypes.ImBack, Value = "I need experts in cloud migration"},
                    //new CardAction { Title = "e.g. What's your phone?", Type = ActionTypes.ImBack, Value = "What's your phone?"},
                }
            };
            return reply;
        }
    }
}