﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Web.Hosting;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity MachineLearningKnowledgeReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.Text = answerText;
            reply.TextFormat = TextFormatTypes.Markdown;
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "How can Acme Inc. help with ML?", Type = ActionTypes.ImBack, Value = "How Acme Inc. can help with ML?"},
                    new CardAction { Title = "Other Acme Inc. services", Type = ActionTypes.ImBack, Value = "Tell me about other Acme Inc. services"}
                }
            };

            return reply;
        }
    }
}