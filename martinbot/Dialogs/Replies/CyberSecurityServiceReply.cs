﻿using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity CyberSecurityServiceReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            var thumbnailCard = new ThumbnailCard
            {
                Title = "Cybersecurity with Acme Inc.",
                Subtitle = "Cybersecurity protects your business. Investing in cybersecurity helps you stay ahead of evolving risks and protects your most sensitive information.",
                Text = answerText,
                Images = new List<CardImage> {new CardImage(Configs.BASE_URL_ASSETS_PUBLIC + "/images/services-cybersec.png")},
                Buttons = new List<CardAction>
                {
                    new CardAction { Title = "learn more »", Type = ActionTypes.OpenUrl, Value = "http://www.acme.com/services/cybersecurity"}
                }

            };
            reply.Attachments = new List<Attachment> { thumbnailCard.ToAttachment()};
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Help", Type = ActionTypes.ImBack, Value = "Help"},
                    new CardAction { Title = "e.g. I wish to get in touch", Type = ActionTypes.ImBack, Value = "I wish to get in touch"}
                }
            };

            return reply;
        }
    }
}