﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity CurrentTimeReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply(answerText);
            reply.TextFormat = TextFormatTypes.Markdown;

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("US Eastern Standard Time");
            DateTime local = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), tzi);

            answerText = answerText.Replace("{{CURRENT_TIME}}", local.ToShortTimeString() + " here in Boston");
            reply.Text = answerText;
            return reply;
        }
    }
}