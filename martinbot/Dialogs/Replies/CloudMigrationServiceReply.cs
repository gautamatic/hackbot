﻿using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity CloudMigrationServiceReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            var thumbnailCard = new ThumbnailCard
            {
                Title = "Cloud with Acme Inc.",
                Subtitle = "Give you business the power to scale quickly, increase security, and enable agility via Acme Inc.’s proven framework for cloud adoption & management.",
                Text = answerText,
                Images = new List<CardImage> {new CardImage(Configs.BASE_URL_ASSETS_PUBLIC + "/images/services-cloud.png")},
                Buttons = new List<CardAction>
                {
                    new CardAction { Title = "learn more »", Type = ActionTypes.OpenUrl, Value = "http://www.acme.com/services/cloud-migration"}
                }

            };
            reply.Attachments = new List<Attachment> { thumbnailCard.ToAttachment()};
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Help", Type = ActionTypes.ImBack, Value = "Help"},
                    new CardAction { Title = "e.g. connect me to an expert", Type = ActionTypes.ImBack, Value = "connect me to an expert"}
                }
            };

            return reply;
        }
    }
}