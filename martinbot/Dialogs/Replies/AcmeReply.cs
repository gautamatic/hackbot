﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity AcmeReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply(answerText);
            reply.TextFormat = TextFormatTypes.Markdown;

            reply.Text = answerText;
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. what services do u offer?", Type = ActionTypes.ImBack, Value = "what services do u offer?"},
                    new CardAction { Title = "e.g. how big is ur team?", Type = ActionTypes.ImBack, Value = "how big is ur team?"},
                    new CardAction { Title = "e.g. who r your tech partners?", Type = ActionTypes.ImBack, Value = "who r your tech partners?"},
                    //new CardAction { Title = "Cloud Migration", Type = ActionTypes.ImBack, Value = "Cloud Migration"},
                    //new CardAction { Title = "Cybersecurity", Type = ActionTypes.ImBack, Value = "Cybersecurity"},
                    //new CardAction { Title = "Software Development", Type = ActionTypes.ImBack, Value = "Software Development"},
                }
            };

            return reply;
        }
    }
}