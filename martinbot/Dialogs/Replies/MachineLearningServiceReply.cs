﻿using martinbot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Web.Hosting;

namespace martinbot.Dialogs
{
    public static partial class Replies
    {
        public static Activity MachineLearningServiceReply(Activity activity, IDialogContext context, string answerText)
        {
            var reply = activity.CreateReply();
            reply.TextFormat = TextFormatTypes.Markdown;

            var thumbnailCard = new ThumbnailCard
            {
                Title = "Machine Learning with Acme Inc.",
                Subtitle = "At Acme Inc., we’re pioneering Machine Learning-first consulting to propel your business",
                Text = answerText,
                Images = new List<CardImage> {new CardImage(Configs.BASE_URL_ASSETS_PUBLIC + "/images/services-ml.png")},
                Buttons = new List<CardAction>
                {
                    new CardAction { Title = "learn more »", Type = ActionTypes.OpenUrl, Value = "http://www.acme.com/services/machine-learning"}
                }

            };
            reply.Attachments = new List<Attachment> { thumbnailCard.ToAttachment()};
            reply.SuggestedActions = new SuggestedActions
            {
                Actions = new List<CardAction>
                {
                    new CardAction { Title = "e.g. Help", Type = ActionTypes.ImBack, Value = "Help"},
                    new CardAction { Title = "e.g. How can I get in touch with u?", Type = ActionTypes.ImBack, Value = "How can I get in touch with u?"}
                }
            };

            return reply;
        }
    }
}